# https with redirect and with hidden service

Notes:

* **onion service access does not involve your TLS certificate in any way**
    * while onion domains *can* be listed in TLS certificates, this is
        uncommon; Let&#x2019;s Encrypt does not support it at all
    * if you *want* HTTPS over Tor, the https-with-redirect deployment is
        already sufficient
    * regardless, configure your Tor to point to the unix domain httpd
* these are service directories intended for supervision with s6
* if a file in `data/env` should be nonempty, the corresponding environment
    variable will be unset, and the script will fail
* the shebangs expect the systemwide execline to have been installed according
    to the [slashpackage](https://cr.yp.to/slashpackage.html) convention
    * if installed normally instead, edit accordingly
* the httpd and http-to-https users should not have write permissions on
    any file or directory within the httpd.execline jail
* create symlinks from the hidden service domain names to the clearnet
    ones or vice versa, and consider configuring domain-wide Onion-Location
    headers for the clearnet domains
