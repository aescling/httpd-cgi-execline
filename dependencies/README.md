Build configurations for httpd.execline userland dependencies, and a build
script to ease deployment
## contents

+ `_9base/Makefile`: mimimally necessary plan 9 userland for httpd.execline,
  and also tr(1)
+ `_toybox`: minimal toybox `.config` files for httpd.execline
    - for a static build, run `LDFLAGS=' -static' make`
    - `toybox/.config`: truly minimal
    - `toybox.config.with-tr`: also builds tr(1), which is pending
+ `build.sh`: build httpd.execline runtime requirements and a TLS server to run
    under
    - installs statically linked binaries into `./_build/bin`
        - builds latest releases
            - exepct 9base, which builds at current git
        - WARNING: toybox build on glibc warns that glibc is needed at runtime
        - NOTE: s6-applyuidgid is needed before chroot; install accordingly
        - NOTE: s6-networking binaries are needed before chroot; install
            accordingly
            - this build script uses BearSSL for TLS support
            - WARNING: s6-tlsserver is overly lax with allowed TLS protocols
                and ciphers
        - WARNING: the resulting binaries are not particularly hardended
    - `build-in-musl-chroot.sh`: prepare a musl chroot, and build musl-linked
        binaries in there
        - requires linux
        - if you are not on an x86_64 architecture, supply your architecture as
            an argument to the script
        - installs statically-linked binaries in `./xbps/_root/_build/bin`