#!/bin/sh -xe
# build the required runtime dependencies and install them into the
# httpd.execline directory
#
# TODO: most of the skarnet binaries are not required
#
# expected to run in this directory
# builds the result into `_build` in this directiory

fail() {
	echo "${0}: fatal: ${@}" 1>&2
	exit 1
}

usage() {
	fail "usage: HTTPD_EXECLINE_ROOT=<chroot jail directory> ${0}"
}

[ ! x"${_CHROOT_BUILD}" = x -a x"${HTTPD_EXECLINE_ROOT}" = x ] && usage
[ ! -r "${HTTPD_EXECLINE_ROOT}" ] && fail "HTTPD_EXECLINE_ROOT (${HTTPD_EXECLINE_ROOT}) is unreadable"

_PWD="$(pwd)"
PREFIX=${_PWD}/_build
[ -d "${PREFIX}" ] || mkdir "${PREFIX}"

SKARNET=git://git.skarnet.org
SKALIBS=${SKARNET}/skalibs
EXECLINE=${SKARNET}/execline
S6_PORTABLE_UTILS=${SKARNET}/s6-portable-utils
S6=${SKARNET}/s6 # required for s6-networking; and s6-applyuidgid, which is run BEFORE chrooting
S6_DNS=${SKARNET}/s6-dns # required for s6-networking
BEARSSL=https://www.bearssl.org/git/BearSSL # optionally required for TLS support in s6-networking
S6_NETWORKING=${SKARNET}/s6-networking
TOYBOX=https://landley.net/toybox/git
_9BASE=https://git.suckless.org/9base

latest() {
	git describe --tags --abbrev=0
}

build() {
	case "${1}" in
		"9base")
			git checkout master # inelegant
			cp -f "${PWD}"/_9base/Makefile .
			make PREFIX="${PREFIX}" install
			;;
		git) # toybox
			cp -f "${PWD}"/_toybox/.config .
			LDFLAGS=" -static" PREFIX="${PREFIX}"/bin make install_flat
			;;
		BearSSL)
			make
			;;
		s6-networking)
			./configure --enable-static-libc --disable-shared --enable-ssl=bearssl --with-include="${PWD}"/BearSSL/inc --with-lib="${PWD}"/BearSSL/build --prefix="${PREFIX}" --with-include="${PREFIX}"/include --with-lib="${PREFIX}"/lib --with-lib="${PREFIX}"/lib/skalibs --with-lib="${PREFIX}"/lib/s6-dns --with-lib="${PREFIX}"/lib/s6
			make install
			;;
		s6)
			./configure --enable-static-libc --disable-shared --prefix="${PREFIX}" --with-include="${PREFIX}"/include --with-lib="${PREFIX}"/lib --with-lib="${PREFIX}"/lib/skalibs --with-lib="${PREFIX}"/lib/execline
			make install
			;;
		*)
			./configure --enable-static-libc --disable-shared --prefix="${PREFIX}" --with-include="${PREFIX}"/include --with-lib="${PREFIX}"/lib --with-lib="${PREFIX}"/lib/skalibs
			make install
			;;
	esac
}

# build
for URL in ${SKALIBS} ${EXECLINE} ${S6} ${S6_PORTABLE_UTILS} ${S6_DNS} ${BEARSSL} ${S6_NETWORKING} ${TOYBOX} ${_9BASE}
do
	(
		PACKAGE="${URL##*/}"
		if ! git -C ${PACKAGE} status
		then
			git clone ${URL}
			cd ${PACKAGE}
		else
			cd ${PACKAGE}
			git fetch
		fi
		git checkout "$(latest)"
		build ${PACKAGE}
	)

done

# install
if [ ! x"${_CHROOT_BUILD}" = x ]
then
	cp -fv $(find -type f "${PREFIX}"/bin) "${HTTPD_EXECLINE_ROOT}"/binaries
fi
