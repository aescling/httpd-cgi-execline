# hidden-service only

Notes:

* the only listening sockets created are in the unix domain
* configure your Tor hidden service(s) appropriately
* these are service directories intended for supervision with s6
* if a file in `data/env` should be nonempty, the corresponding environment
    variable will be unset, and the script will fail
* the shebangs expect the systemwide execline to have been installed according
    to the [slashpackage](https://cr.yp.to/slashpackage.html) convention
    * if installed normally instead, edit accordingly
* the httpd user should not have write permissions on
    any file or directory within the httpd.execline jail
